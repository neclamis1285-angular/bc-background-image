(function() {

  angular.module('bcBackgroundImage')
    .directive('bcBackgroundImage', function() {

      return function(scope, element, attrs) {
        attrs.$observe('bcBackgroundImage', function(value) {
          if (value) {
            element.css({
              'background-image': 'url(' + value + ')',
              'background-size': 'cover'
            });
          }
        });
      };

    });

})(angular.module("bcBackgroundImage", []));
